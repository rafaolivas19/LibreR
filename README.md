## Compilar

1. Clona

```elm
git clone https://gitlab.com/rafaolivas19/LibreR.git
```

1. Ve a la raíz del proyecto

```elm
cd LibreR/
```

1. Compila
    
    Abre la solución con Visual Studio, en el `Explorador de soluciones` da click derecho sobre la solución y selecciona `Recompilar solución`

## Instalar

1. Clona el repo Nuget de LYF

    ```elm
    git clone https://gitlab.com/lyf-ingenieria/utils/nuget.git
    ```

1. Agrega a Visual Studio el repositorio Nuget ([guía](https://lyf-ingenieria.gitlab.io/docs/lecciones-aprendidas/Gu%C3%ADas%20%26%20Tutoriales/nuget/))
1. En Visual Studio ve al proyecto en el `Explorador de soluciones`, en la sección `Dependencias` da click derecho y ve a `Administrar paquetes NuGet...`

    ![](https://trello-attachments.s3.amazonaws.com/599b30604a55ce787c9a0157/5fd9571970a415858bb0137b/3286641a9f9126960f27a78e18c8d9aa/image.png)

1. En la opción `Origen del paquete` selecciona el repositorio de LYF (que configuraste en el paso 2) y en la pestaña `Examinar` busca el paquete que quieres instalar

    ![](https://trello-attachments.s3.amazonaws.com/599b30604a55ce787c9a0157/5fd9571970a415858bb0137b/4a60fa713f06c42634884dd27d2bfcef/image.png)

## Publicar

1. Actualiza el número de versión

    1. `LibreR/LibreR.csproj`

        ```xml
        ...
        <AssemblyVersion>1.0.0</AssemblyVersion>
        <Version>1.0.0</Version>
        <FileVersion>1.0.0</FileVersion>
        <InformationalVersion>1.0.0</InformationalVersion>
        ...
        ```

1. Compila el proyecto para producción

    `Revisa la sección "Compilar".`

1. Toma el paquete NuGet generado `LibreR/bin/Release/LibreR.*.*.*.nupkg` y agregalo al [repo Nuget de LYF](https://gitlab.com/lyf-ingenieria/utils/nuget)
